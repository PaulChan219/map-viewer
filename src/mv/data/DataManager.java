package mv.data;


import javafx.scene.shape.Polygon;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import saf.components.AppDataComponent;
import mv.MapViewerApp;

/**
 *
 * @author McKillaGorilla
 */
public class DataManager implements AppDataComponent {
    MapViewerApp app;
    int NumOfSubregions;
    ObservableList<Polygon> poly;
    
    public DataManager(MapViewerApp initApp) {
        app = initApp;
        poly  = FXCollections.observableArrayList();
    }
      public ObservableList<Polygon> getItems() {
	return poly;
    }
       public void addItem(Polygon item) {
        poly.add(item);
    }
       public void clearList(){
           poly.clear();
       }
    @Override
    public void reset() {
        
        this.clearList();
    }
}
