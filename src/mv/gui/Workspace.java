/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mv.gui;

import java.io.IOException;
import javafx.collections.ObservableList;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;
import saf.components.AppWorkspaceComponent;
import mv.MapViewerApp;
import mv.data.DataManager;
import saf.AppTemplate;
import saf.ui.AppGUI;

/**
 *
 * @author McKillaGorilla
 */
public class Workspace extends AppWorkspaceComponent {
    MapViewerApp app;
    DataManager dataManager;
    ObservableList<Polygon> polygonlist;
    AppTemplate apps;
    AppGUI gui;
    Boolean G;
    public Workspace(MapViewerApp initApp)  throws IOException {
        app = initApp;
        workspace = new Pane();
        gui = app.getGUI();
        G=false;
       dataManager = (DataManager)app.getDataComponent();
       setupHandlers();
       RenderTheImage();
    }
    public void RenderTheImage(){
        workspace.setStyle("-fx-background-color: blue;");
        polygonlist.addAll(dataManager.getItems()) ;
        for(int i= 0;i< polygonlist.size();i++){
            Polygon X = polygonlist.get(i);
            X.setFill(Color.GREEN);
            X.setStroke(Color.BLACK);
            X.setStrokeWidth(1);
            workspace.getChildren().add(X);
        }
       
    }
    public void setupHandlers() {
        workspace.setOnMouseClicked(e->{
            if(G=false){
                G=true;
            }
	     if(G=true){
                 G=false;
             }
	});
    //    if (event.getKeyCode() == KeyEvent.VK_UP) {

     //   }
     ///   if (event.getKeyCode() == KeyEvent.VK_DOWN) {

      ///  }
      ///  if (event.getKeyCode() == KeyEvent.VK_LEFT) {

     ///   }
     ////   if (event.getKeyCode() == KeyEvent.VK_RIGHT) {

      //  }
      double h =workspace.getHeight();
      double lath =h/6;
      double w =workspace.getWidth();
      double logw =w/12;
      //double startH =0;
      //double startW =0;
      
      for ( double startH =0; startH<h;startH=startH+lath){
          Line line =new Line(startH , 0, startH, w);
          if(startH==h/2){
              Line line1 =new Line(startH , 0, startH, w);
              
          }
      }
      for ( double startW =0; startW<w;startW=startW+logw){
          Line line =new Line(0 , startW, h,startW);
          if(startW==h/2){
              Line line1 =new Line(0 , startW, h,startW);
              
          }
      }
    }
    
    @Override
    public void reloadWorkspace() {
  //      workspace= new Pane();
  //      workspace.getChildren().clear();
        
    }

    @Override
    public void initStyle() {
        
    }
}
