/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mv.file;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.shape.Polygon;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import mv.data.DataManager;
import saf.components.AppDataComponent;
import saf.components.AppFileComponent;

/**
 *
 * @author McKillaGorilla
 */
public class FileManager implements AppFileComponent {
    //This is an int
    static final String JSON_NUMBER_OF_SUBREGIONS = "NUMBER_OF_SUBREGIONS";
    //This is an object that list
    static final String JSON_SUBREGIONS = "SUBREGIONS";
    //this is an int
    static final String JSON_NUMBER_OF_SUBREGION_POLYGONS = "NUMBER_OF_SUBREGION_POLYGONS";
    //this is an object with a list of arrays
    static final String JSON_SUBREGION_POLYGONS = "SUBREGION_POLYGONS";
    //ints
    static final String JSON_X = "X";
    static final String JSON_Y = "Y";
    
    
    
    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        // CLEAR THE OLD DATA OUT
        DataManager dataManager = (DataManager)data;
	dataManager.reset();
        
        // LOAD THE JSON FILE WITH ALL THE DATA
        JsonObject json = loadJSONFile(filePath);
               
        int NumberOfSubregions = getDataAsInt(json,JSON_NUMBER_OF_SUBREGIONS);
        JsonArray SubregionsArray = json.getJsonArray(JSON_SUBREGIONS);

       // loop for subregion array
	for (int i = 0; i < SubregionsArray.size(); i++) {  
            //object with subregion poly array, and number of arrays
            JsonObject Subregions = SubregionsArray.getJsonObject(i);
            //"NUMBER_OF_SUBREGION_POLYGONS":#
            int NumberOfSubregionPolygons =getDataAsInt(Subregions,JSON_NUMBER_OF_SUBREGION_POLYGONS);
            //"SUBREGION_POLYGONS":[
            JsonArray SubregionPolygonsArray = Subregions.getJsonArray(JSON_SUBREGION_POLYGONS);
            // loop for subregion polygon array
            for (int j = 0; j < NumberOfSubregionPolygons; j++) {
                //strange array style
                JsonArray array = SubregionPolygonsArray.getJsonArray(j);
                //used to hold xy coordinats
                ObservableList<Double> listxy = FXCollections.observableArrayList();
                //loop to break first array
                for (int k = 0; k < array.size(); k++) {
                  //loop to get x,y
                    
                        JsonObject point = array.getJsonObject(k);
                        double x = getDataAsDouble(point,JSON_X);
                        double y = getDataAsDouble(point,JSON_Y);
                        
                    listxy.addAll(x,y);
                }
            Polygon polygons = new Polygon();
            polygons.getPoints().addAll(listxy);
            dataManager.addItem(polygons);
            }	   
	}
    }
    
    public double getDataAsDouble(JsonObject json, String dataName) {
	JsonValue value = json.get(dataName);
	JsonNumber number = (JsonNumber)value;
	return number.bigDecimalValue().doubleValue();	
    }
    
    public int getDataAsInt(JsonObject json, String dataName) {
        JsonValue value = json.get(dataName);
        JsonNumber number = (JsonNumber)value;
        return number.bigIntegerValue().intValue();
    }
    
    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }

    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


}
